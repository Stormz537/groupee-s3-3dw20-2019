// exécution des fonctions javascript après chargement du document HTML
$(document).ready(function() {

	//déclaration des variables :
	var $jours = $('.jour');
	var $puces = $('.bullets .entypo-record');

	function init() {	// initialisation des paramètres indiqué dans la fonction
		setTimeout(function() { // exécution de la fonction toutes 2000 milisecondes
			$('body').addClass('isok'); //ajout d'une classe isok à l'élément html body.
			$jours.hide(); //Cache l'élément jours
			$('.wrapper').fadeIn('slow', function() { //effectue une animation de type "fondu" doucement
				$jours.first().fadeIn('slow'); // animation lente de type "fondu" du premier jour
				$puces.removeClass('active').first().addClass('active'); // .first --> .first()
				// suppression de la classe active de la variable puces et ajout de la première classe active.
			});
		}, 2000);
	}

	$puces.click(function() { 	// correction $puce -> $puces
					//exécution de la fonction lorsqu'on clique sur l'élément puce
		var $this = $(this); //déclaration de la variable, élément que l'utilisateur va selectionner
		var cible = $this.attr('data-cible'); // déclaration de cible indiquant les valeur récupéré de 'data-cible' sélectionné par l'utilisateur
		$jours.hide(); //Cache l'élément jours
		$($jours.get(cible)).fadeIn(); //récupération des valeurs cible  et affichage de jour
		$puces.removeClass('active'); //suppression de la classe active de $puces
		$this.addClass('active'); // ajout de la classe active élément selectioné par l'utilisateur
	});
	//lancement de la fonction init déclarée en amont:
	init(); 
});  